# Minimal TeX Install (for Linux)

This script installs a minimal TeX installation for the local user and the local user only. It does not install the dependencies.

This script is only designed to work for Linux, but if the profile files are modified it is easily extendable for other OSes.

## Dependencies

You need to have `tar`, `curl`, and `perl` on your system.
