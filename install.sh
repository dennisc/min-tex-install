# If your libc is GNU libc, use this command
# Almost every Linux distro is on GNU libc, so if you don't know you're probably using glibc.
wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar -xf install-tl-unx.tar.gz
rm install-tl-unx.tar.gz
TL=$(ls | grep install-tl)
./$TL/install-tl --profile glibc.profile
export PATH=$HOME/.local/texlive/bin
tlmgr install texliveonfly
