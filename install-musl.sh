# If your libc is musl libc, use this command
# musl libc systems include Alpine
wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar -xf install-tl-unx.tar.gz
rm install-tl-unx.tar.gz
TL=$(ls | grep install-tl)
./$TL/install-tl --profile musl.profile
PATH=$HOME/.local/texlive/bin
tlmgr install texliveonfly
